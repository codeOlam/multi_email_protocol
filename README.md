# Python Multi-eMailing Protocol script

### About
This script allows for sending multiple and customized e-mail from any email providers available samultaneously. Merging the email address with the appropriate salutations, thereby, personalizing the email.

# Getting Started

### Install python and virtual environment
1. visit python.org and follow the instructions on who to install and setup python on your computer.
2. install virtual environment. this is optional but advisable to do
follow the link bellow to setup virtual environment on your system
	1. using windows power shell:

		https://thinkdiff.net/python/how-to-install-python-virtualenv-in-windows/

	2. Creating Python Virtual Environment in Windows and Linux

		https://www.geeksforgeeks.org/creating-python-virtual-environment-windows-linux/

### Install Packages
run this command on your terminal to install the following package to handle the excel sheet
```
	pip install openpyxl
```

### Clone Repo
clone the repository from your terminal

```
	git clone https://codeOlam@bitbucket.org/codeOlam/multi_email_protocol.git
```

### Setup Your Content of E-mail and the Excel to hold contact e-mail address and sulations
targeting the salutation

1. Open your text editor and type in the content of the email.
2. Use python string Template formating for string substitution for salution customaization.
	
	```
		${identifier}
	```

3. edit this line for the particular postions you wish to customize
	
	```
		body = mail_template.substitute(identifier=salutation.title())
	```

4. use the sample excel sheet on the repo and populate your contact email address and their approprate salutations
5. save the content of the email with extention of ".txt" and the excel with extention of ".xlsx". then edit the following lines with the name of the 
	
	```	
		wb = load_workbook('./the_excel.xlsx')
	```


	```
		salutations, emails = read_contacts('the_excel.xlsx')
	```


	```
		mail_template = read_template('.txt')
	```


# Run Script
from the command line run:
```
	python psmep3.py
```
or for python3
``` 
	python3 psmep3.py
```

## NB
1. you can use the same *.txt* file and the sample *.xlsx* and edit it to suit your need from the repo.
2. make sure they are all on same directory before running your python script

#Technologies
Python 2.x or 3.x

# Resources
1. https://www.python.org/
2. https://docs.python.org/2.4/lib/node109.html
3. https://thinkdiff.net/python/how-to-install-python-virtualenv-in-windows/
4. https://www.geeksforgeeks.org/creating-python-virtual-environment-windows-linux/
