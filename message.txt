${PERSON_NAME}

Heading of E-mail

My journey began years ago, and it has continued thus far only by your support. It was you who ensured that my dream was realized in several ways including supporting me financially, morally, spiritually, and emotionally. Several times, my dream seemed far away and should have been aborted. However, knowing that you were there kept me surging forward. Alas, the time has come for me to share my knowledge.
 
I do not see any other way to honor and acknowledge your contributions to my life than to invite you to....
