"""
    This is a python script for multiple email protocol

    send customized emails to multiple contacts.

    @Author: Frank U.N.
    @Date: 18/11/2017
    @Version: 1.0

"""

import smtplib
from openpyxl import load_workbook
from string import Template
try:
    #for python2
    from email.MIMEMultipart import MIMEMultipart
    from email.MIMEText import MIMEText
except:
    #for python3
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
import codecs


def read_contacts(contactdetails):
    """
    this function is to read the contact from an excel sheet
    and return a list of salutaion and email addresses
    :param contactdetails: mycontacts.xlsx
    :return: slautaions and emails
    """

    # load the spread sheet
    wb = load_workbook('./the_excel.xlsx')

    # go to the active sheet
    #ws = wb.active
    sheet = wb.get_sheet_by_name('s')


    # initializing the columns
    first_column = sheet["A"]
    second_column = sheet["B"]

    # Array to store the salutaions and emails
    values_A = []
    values_B = []

    for x in xrange(1, len(first_column)):
        values_A.append(first_column[x].value)
        values_B.append(str(second_column[x].value).lower())

    return values_A, values_B


def read_template(text):
    """
    this function reads in a tamplate file
    and returns object made from it
    :param text: message.txt
    :return: template_content
    """

    body_text = codecs.open(text, mode='r', encoding='utf-8')

    template_content = body_text.read()

    return Template(template_content)


def main():

    # Declarations
    host = 'smtp.gmail.com'
    port = '587'
    pswd = ""
    fromaddr = ""
    salutations, emails = read_contacts('the_excel.xlsx')
    mail_template = read_template('.txt')

    # Setting up the SMTP
    server = smtplib.SMTP(host, port)
    server.starttls()
    server.login(fromaddr, pswd)

    for salutation, email in zip(salutations, emails):
        # compose a message
        msg = MIMEMultipart()

        # sub actual salutations
        body = mail_template.substitute(PERSON_NAME=salutation.title())

        # setting up mail parameters
        msg['From'] = fromaddr
        msg['To'] = email
        msg['Subject'] = ""

        # mail body
        msg.attach(MIMEText(body, 'plain'))
        text = msg.as_string()
        server.sendmail(fromaddr, email, text)

    # end the smtp session and close connection
    server.quit()


if __name__ == '__main__':
    main()
